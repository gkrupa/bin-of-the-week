package uk.me.gjk.bins

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import reactor.core.scheduler.Schedulers
import uk.me.gjk.bins.council.CouncilClient
import uk.me.gjk.bins.date.NextApplicableDateCalculator
import uk.me.gjk.bins.push.PushoverClient
import java.time.LocalDate

private val log = KotlinLogging.logger {  }

@Component
class Scheduler(
    private val councilClient: CouncilClient,
    private val nextApplicableDateCalculator: NextApplicableDateCalculator,
    private val pushoverClient: PushoverClient,
    @Value("\${council.notifier.street}") private val streetName: String
) {

    @Scheduled(cron = "0 30 17 * * WED", zone = "Europe/London")
    fun checkNextBin() {
        log.info("Running schedule")
        val now = LocalDate.now()
        councilClient.pageForStreetName(streetName)
            .flatMap(councilClient::datesForCollectionUrl)
            .map { nextApplicableDateCalculator.next(it, now) }
            .flatMap {
                log.info("Sending notification")
                pushoverClient.sendMessage("""
                    It's bin day tomorrow. You need to put out the following bins:
                    ${it.joinToString(", ")}
                """.trimIndent())
            }.subscribeOn(Schedulers.parallel()).subscribe()
    }

}
