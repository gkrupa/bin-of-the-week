package uk.me.gjk.bins

import com.github.benmanes.caffeine.cache.Caffeine
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.cache.CacheMono
import reactor.core.publisher.Mono
import reactor.core.publisher.Signal
import uk.me.gjk.bins.council.CouncilClient
import uk.me.gjk.bins.date.NextApplicableDateCalculator
import java.time.LocalDate
import java.util.concurrent.TimeUnit

typealias DateMapping = Map<LocalDate,List<String>>

@RestController
class RestController(private val councilClient: CouncilClient, private val nextApplicableDateCalculator: NextApplicableDateCalculator) {

    val cache = Caffeine.newBuilder()
        .maximumSize(1000)
        .expireAfterWrite(5, TimeUnit.DAYS)
        .build<String,Signal<out DateMapping>>()

    @GetMapping("/street")
    fun street(@RequestParam street: String): Mono<List<String>> =
            CacheMono.lookup(cache.asMap(), street)
                .onCacheMissResume(retrieveFromWeb(street))
                .map { nextApplicableDateCalculator.next(it, LocalDate.now()) }

    fun retrieveFromWeb(street: String) = councilClient.pageForStreetName(street)
            .flatMap(councilClient::datesForCollectionUrl)
}
