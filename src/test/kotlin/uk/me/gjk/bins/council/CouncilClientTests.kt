package uk.me.gjk.bins.council

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.util.StreamUtils
import uk.me.gjk.bins.parser.HtmlParser
import java.nio.charset.Charset

const val DIRECTORY_ID: Int = 999

const val STREET_NAME = "Genuine Lane"

class CouncilClientTests {

    private val server = WireMockServer()

    private var subjectUnderTest: CouncilClient? = null

    @BeforeEach
    fun init() {
        server.start()
        val initialUrl = "http://localhost:${server.port()}/search"
        subjectUnderTest = CouncilClient(initialUrl, DIRECTORY_ID, HtmlParser())
    }

    @AfterEach
    fun afterEach() {
        server.stop()
    }

    @Test
    fun `Page result is detected`() {
        stubFor(
                get(urlPathEqualTo("/search"))
                        .withQueryParam("directoryID", WireMock.equalTo(DIRECTORY_ID.toString()))
                        .withQueryParam("search", WireMock.equalTo("Go"))
                        .withQueryParam("keywords", WireMock.equalTo(STREET_NAME))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(StreamUtils.copyToString(javaClass.classLoader.getResourceAsStream("search-result-single.html"), Charset.defaultCharset()))))

        stubFor(
                get("/directory_record/34690/genuine_lane")
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(StreamUtils.copyToString(javaClass.classLoader.getResourceAsStream("street-details-valid.html"), Charset.defaultCharset()))))

        assertThat(subjectUnderTest?.pageForStreetName(STREET_NAME)?.block(), equalTo("http://www.coventry.gov.uk/binsthursdayb"))
    }

    @Test
    fun `No results page throws an exception`() {
        stubFor(
                get(urlPathEqualTo("/search"))
                        .withQueryParam("directoryID", WireMock.equalTo(DIRECTORY_ID.toString()))
                        .withQueryParam("search", WireMock.equalTo("Go"))
                        .withQueryParam("keywords", WireMock.equalTo(STREET_NAME))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(StreamUtils.copyToString(javaClass.classLoader.getResourceAsStream("search-result-no-match.html"), Charset.defaultCharset()))))

        Assertions.assertThrows(java.lang.RuntimeException::class.java, {
            subjectUnderTest!!.pageForStreetName(STREET_NAME).block()
        }, "No such street found")
    }

    @Test
    fun `Search page error throws an exception`() {
        stubFor(
                get(urlPathEqualTo("/search"))
                        .withQueryParam("directoryID", WireMock.equalTo(DIRECTORY_ID.toString()))
                        .withQueryParam("search", WireMock.equalTo("Go"))
                        .withQueryParam("keywords", WireMock.equalTo(STREET_NAME))
                        .willReturn(aResponse()
                                .withStatus(404)
                        )
        )

        Assertions.assertThrows(java.lang.RuntimeException::class.java, {
            subjectUnderTest!!.pageForStreetName(STREET_NAME).block()
        }, "No such street found")
    }

    @Test
    fun `Details page error throws an exception`() {
        stubFor(
                get(urlPathEqualTo("/search"))
                        .withQueryParam("directoryID", WireMock.equalTo(DIRECTORY_ID.toString()))
                        .withQueryParam("search", WireMock.equalTo("Go"))
                        .withQueryParam("keywords", WireMock.equalTo(STREET_NAME))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(StreamUtils.copyToString(javaClass.classLoader.getResourceAsStream("search-result-single.html"), Charset.defaultCharset()))))

        stubFor(
                get("/directory_record/34690/genuine_lane")
                        .willReturn(aResponse()
                                .withStatus(502)
                        )
        )

        Assertions.assertThrows(java.lang.RuntimeException::class.java, {
            subjectUnderTest!!.pageForStreetName(STREET_NAME).block()
        }, "Details page did not exist" )
    }

    @Test
    fun `Page result is first from multiple results`() {
        val streetPrefix = STREET_NAME.split(" ")[0]
        stubFor(
                get(urlPathEqualTo("/search"))
                        .withQueryParam("directoryID", WireMock.equalTo(DIRECTORY_ID.toString()))
                        .withQueryParam("search", WireMock.equalTo("Go"))
                        .withQueryParam("keywords", WireMock.equalTo(streetPrefix))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(StreamUtils.copyToString(javaClass.classLoader.getResourceAsStream("search-result-multiple.html"), Charset.defaultCharset()))))

        stubFor(
                get("/directory_record/34690/genuine_lane")
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html")
                                .withBody(StreamUtils.copyToString(javaClass.classLoader.getResourceAsStream("street-details-valid.html"), Charset.defaultCharset()))))

        assertThat(subjectUnderTest!!.pageForStreetName(streetPrefix).block(), equalTo("http://www.coventry.gov.uk/binsthursdayb"))
    }
}
